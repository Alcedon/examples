const http = require('http'),			// HTTP API
	  path = require('path'),			// API for paths on the filesystem
	  fs = require('fs'), 		    	// API for file system access
	  mimedb = require('mime-db');		// we need the mime database to map file extensions to content types

// definition of a root directory (this should normally be in a config file)
// the user can only read data from this directory with GET requests, like
// the web directory of a apache server
// -> see further code for usage
const rootdir = 'html';
const rootfull = path.resolve(rootdir); // get absolute for root dir

// formatted mime database
let mime = reformatMimeDb();

// create a new HTTP server and give a function
let server = http.createServer(onrequest);

// start the server on port 12345
server.listen(12345);

function reformatMimeDb() {
	let extensions = {};
	for(let type in mimedb) {
		// ignore mime types if there is no extension related
		if (!mimedb[type].extensions) {
			continue;
		}

		// build an object with the file extension (for example .html)
		// as key and the mime type as value
		for(let ext of mimedb[type].extensions) {
			extensions['.' + ext] = type;
		}
	}
	return extensions;
}

// this function will be called on every request
function onrequest(request, response) {
	// request: contains the request data (ip address, method, header, etc.)
	// response: to send data back to the client use this object

	// for this example we allow only GET requests
	if (request.method !== 'GET') {
		// write HTTP header
		response.writeHead(405, 'method not allowed');
		// close connection
		response.end('method not allowed');
		return;
	}

	// build the path for the file by concatenating the root directory and the url
	// if the client access the domain without a path we supply the file index.html
	let url = request.url === '/' ? '/index.html' : request.url,
		joinedpath = path.join(rootfull, url),
		absolutePath = path.resolve(joinedpath);

	// check if the file is inside the root directory
	if (!absolutePath.startsWith(rootfull)) {
		// write HTTP header
		response.writeHead(404, 'file not found');
		// close connection
		response.end('file not found');
		return;
	}

	// at this point we now the client send a GET request and tries to
	// access a file inside the root directory -> everything is ok


	try {
		// open the file for reading and supply a function
		// also bind the path and response object to the function
		fs.open(absolutePath, 'r', onfileopened.bind(null, absolutePath, response));
	} catch (e) {
		// an error occured
		response.writeHead(404, 'file not found')
		// close connection
		response.end('file not found');
		return;
	}
}

function onfileopened(filepath, response, error, filedescriptor) {
	if (error) {
		// an error occured
		response.writeHead(404, 'file not found')
		// close connection
		response.end('file not found');
		return;
	}
	// ok, the file exists and we can access it

	// we have to send the content type to the client
	// -> html, js, css, etc.
	// to get the content type we use the file extension
	let extension = path.extname(filepath),
		type = mime[extension];

	// unknown extension -> the client can download the file
	if (!type) {
		type = 'application/octet-stream';
	}

	// send a 200 status code and set the HTTP headers
	response.writeHead(200, {
		'Content-Type': type
	});

	// create a stream reader
	let reader = fs.createReadStream(null, {fd: filedescriptor});
	// forward the data to the client
	reader.pipe(response);
}
